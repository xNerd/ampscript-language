# Change Log

All notable changes to the "ampscript-language" extension will be documented in this file.

## [Unreleased]

- Snippets
- Code folding on if / for statements

## [1.0.0] - 2019-09-25
### Added

- Initial release
- AMPscript highlighting and commenting
- HTML, JS, and CSS highlighting and commenting

[Unreleased]: https://gitlab.com/xNerd/ampscript-language/compare/master...v1.0.0
[1.0.0]: https://gitlab.com/xNerd/ampscript-language/-/tags/v1.0.0