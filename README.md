## Features

* Syntax Highlighting for AMPscript language
* Includes Highlighting and Commenting for HTML, CSS, and JS

## Release Notes

### 1.0.0

Initial release, includes HTML, JS, and CSS highlighting and commenting

## AMPscript Docs

https://developer.salesforce.com/docs/atlas.en-us.noversion.mc-programmatic-content.meta/mc-programmatic-content/languageElements.htm

### Contact

Email nerd@multiplytechnology.com for questions and/or pull requests
